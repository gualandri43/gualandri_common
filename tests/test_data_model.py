import os
import io
import csv
import uuid
import datetime
import unittest
import dataclasses
import uuid
import lxml.etree
import psycopg2
import psycopg2.extras
import gualandri_common.data_model as data_model

@dataclasses.dataclass
class Data1(data_model.DataModelBase):
    _schema_name = 'schema1'
    _table_name = 'table1'

    field1: float
    field2: str

@dataclasses.dataclass
class Data2(data_model.DataModelBase):
    _schema_name = 'schema2'
    _table_name = 'table2'

    field1: int
    field2: Data1

@dataclasses.dataclass
class DataOddTypes(data_model.DataModelBase):
    _schema_name = 'schema3'
    _table_name = 'table3'

    field1: float
    field2: str
    field3: uuid.UUID
    field4: datetime.datetime
    field5: datetime.date

class DataModelBaseTest(unittest.TestCase):

    def test__snake_to_camel_case(self):

        self.assertEqual(
            data_model.DataModelBase._snake_to_camel_case('my_test_string'),
            'myTestString'
        )

        self.assertEqual(
            data_model.DataModelBase._snake_to_camel_case('this_string'),
            'thisString'
        )

        self.assertEqual(
            data_model.DataModelBase._snake_to_camel_case('single'),
            'single'
        )

        self.assertEqual(
            data_model.DataModelBase._snake_to_camel_case('alreadyCamel'),
            'alreadyCamel'
        )

        self.assertEqual(
            data_model.DataModelBase._snake_to_camel_case('Capital_snake'),
            'capitalSnake'
        )

    def test__camel_to_snake_case(self):

        self.assertEqual(
            data_model.DataModelBase._camel_to_snake_case('myTestString'),
            'my_test_string'
        )

        self.assertEqual(
            data_model.DataModelBase._camel_to_snake_case('thisString'),
            'this_string'
        )

        self.assertEqual(
            data_model.DataModelBase._camel_to_snake_case('single'),
            'single'
        )

        self.assertEqual(
            data_model.DataModelBase._camel_to_snake_case('already_snake'),
            'already_snake'
        )

        self.assertEqual(
            data_model.DataModelBase._camel_to_snake_case('CapitalCamel'),
            'capital_camel'
        )

    def test_create_empty(self):

        d1 = Data1(_id=None, custom_data={}, field1=None, field2=None)
        d2 = Data2(_id=None, custom_data={}, field1=None, field2=d1)

        self.assertEqual(Data1.create_empty(), d1)
        self.assertEqual(Data2.create_empty(), d2)

    def test_create_from_existing(self):

        d1 = Data1.create_empty()
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2.field1 = 4
        d2.field2 = d1

        self.assertEqual(d1, Data1.create_from_existing(d1))
        self.assertEqual(d2, Data2.create_from_existing(d2))

    def test_create_from_dict(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {}
        d2.field1 = 4
        d2.field2 = d1

        self.assertEqual(
            d2,
            Data2.create_from_dict({
                '_id': 2,
                'custom_data': {},
                'field1': 4,
                'field2': d1.to_dict()
            })
        )

    def test_to_dict(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {}
        d2.field1 = 4
        d2.field2 = d1

        self.assertEqual(
            d1.to_dict(),
            {
                '_id': 1,
                'custom_data': {},
                'field1': 5.0,
                'field2': 'hi'
            }
        )

        self.assertEqual(
            d2.to_dict(),
            {
                '_id': 2,
                'custom_data': {},
                'field1': 4,
                'field2': d1.to_dict()
            }
        )

    def test_to_json(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {}
        d2.field1 = 4
        d2.field2 = d1

        self.assertEqual(
            d1.to_json(),
            '{"_id": 1, "custom_data": {}, "field1": 5.0, "field2": "hi"}'
        )

        self.assertEqual(
            d2.to_json(),
            '{"_id": 2, "custom_data": {}, "field1": 4, "field2": ' + d1.to_json() + '}'
        )

    def test_json_odd_datatypes(self):

        d1 = DataOddTypes.create_empty()
        d1._id = 1
        d1.custom_data = {"c1":"v1"}
        d1.field1 = 5.0
        d1.field2 = 'hi'
        d1.field3 = uuid.UUID("8ed6392b-be3c-4eb2-947a-18a391783242")
        d1.field4 = datetime.datetime(2020, 11, 10, 14, 42, 34)
        d1.field5 = datetime.date(2020, 11, 13)

        self.assertEqual(
            d1.to_json(),
            '{"_id": 1, "custom_data": {"c1": "v1"}, "field1": 5.0, "field2": "hi", "field3": "8ed6392b-be3c-4eb2-947a-18a391783242", "field4": "2020-11-10T14:42:34", "field5": "2020-11-13"}'
        )

        d2 = d1.create_from_json(d1.to_json())
        self.assertEqual(d1,d2)

    def test_create_from_json(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {}
        d2.field1 = 4
        d2.field2 = d1

        d1Json = '{"_id": 1,"custom_data": {},"field1": 5.0,"field2": "hi"}'
        d2Json = '{"_id": 2,"custom_data": {},"field1": 4,"field2": ' + d1.to_json() + '}'

        self.assertEqual(d1, Data1.create_from_json(d1Json))
        self.assertEqual(d2, Data2.create_from_json(d2Json))

    def test_build_select_sql_postgres(self):

        expectedSql10 = 'SELECT * FROM schema1.table1 AS table1;'
        expectedSql11 = 'SELECT * FROM schema1.table1 AS table1 WHERE _id=1;'
        expectedSql20 = 'SELECT * FROM schema2.table2 AS table2;'
        expectedSql21 = 'SELECT * FROM schema2.table2 AS table2 WHERE _id=2;'

        self.assertEqual(expectedSql10, Data1.build_select_sql_postgres())
        self.assertEqual(expectedSql11, Data1.build_select_sql_postgres(recordId=1))
        self.assertEqual(expectedSql20, Data2.build_select_sql_postgres())
        self.assertEqual(expectedSql21, Data2.build_select_sql_postgres(recordId=2))

    def test_build_insert_sql_postgres(self):

        expectedSql10 = 'INSERT INTO schema1.table1 (_id,custom_data,field1,field2) VALUES (%(_id)s,%(custom_data)s,%(field1)s,%(field2)s) ON CONFLICT (_id) DO UPDATE SET _id=EXCLUDED._id,custom_data=EXCLUDED.custom_data,field1=EXCLUDED.field1,field2=EXCLUDED.field2 RETURNING _id;'
        expectedSql11 = 'INSERT INTO schema1.table1 (_id,field1) VALUES (%(_id)s,%(field1)s) ON CONFLICT (_id) DO UPDATE SET _id=EXCLUDED._id,field1=EXCLUDED.field1 RETURNING _id;'

        expectedSql20 = 'INSERT INTO schema2.table2 (_id,custom_data,field1,field2) VALUES (%(_id)s,%(custom_data)s,%(field1)s,%(field2)s) ON CONFLICT (_id) DO UPDATE SET _id=EXCLUDED._id,custom_data=EXCLUDED.custom_data,field1=EXCLUDED.field1,field2=EXCLUDED.field2 RETURNING _id;'
        expectedSql21 = 'INSERT INTO schema2.table2 (field2) VALUES (%(field2)s) ON CONFLICT (_id) DO UPDATE SET field2=EXCLUDED.field2 RETURNING _id;'

        self.assertEqual(expectedSql10, Data1.build_insert_sql_postgres())
        self.assertEqual(expectedSql11, Data1.build_insert_sql_postgres(fieldsToInclude=['_id','field1']))
        self.assertEqual(expectedSql20, Data2.build_insert_sql_postgres())
        self.assertEqual(expectedSql21, Data2.build_insert_sql_postgres(fieldsToInclude=['field2']))

    def test_build_csv_writer(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {'c1':'bye'}
        d2.field1 = 4
        d2.field2 = d1

        outStr = io.StringIO(newline='')
        writer = Data2.build_csv_writer(outStr)

        self.assertEqual(
            outStr.getvalue().strip(),
            '_id,custom_data,field1,field2._id,field2.custom_data,field2.field1,field2.field2'
        )

        self.assertTrue(isinstance(writer, csv.DictWriter))

    def test_to_csv(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {'c1':'bye'}
        d2.field1 = 4
        d2.field2 = d1

        outStr = io.StringIO(newline='')
        writer = Data2.build_csv_writer(outStr)
        d2.to_csv(writer)

        self.assertEqual(
            outStr.getvalue().strip(),
            '_id,custom_data,field1,field2._id,field2.custom_data,field2.field1,field2.field2\r\n2,"{""c1"": ""bye""}",4,1,,5.0,hi'
        )

    def test_to_xml(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {'c1':'bye'}
        d2.field1 = 4
        d2.field2 = d1


        self.assertEqual(
            lxml.etree.tostring(d2.to_xml()),
            b'<Data2 _id="2"><custom_data>{\'c1\': \'bye\'}</custom_data><field1>4</field1><field2><Data1 _id="1"><custom_data>{}</custom_data><field1>5.0</field1><field2>hi</field2></Data1></field2></Data2>'
        )

class DataModelBaseDbTest(unittest.TestCase):

    def setUp(self):

        self.con = psycopg2.connect(
            host=os.getenv('POSTGRES_HOST'),
            port=os.getenv('POSTGRES_PORT'),
            user=os.getenv('POSTGRES_USER'),
            password=os.getenv('POSTGRES_PASSWORD')
        )

        self.con.autocommit = True
        with self.con.cursor() as curs:
            curs.execute('DROP DATABASE IF EXISTS gualandri_common_testing;')
            curs.execute('CREATE DATABASE gualandri_common_testing;')
        self.con.close()

        self.con = psycopg2.connect(
            host=os.getenv('POSTGRES_HOST'),
            port=os.getenv('POSTGRES_PORT'),
            dbname='gualandri_common_testing',
            user=os.getenv('POSTGRES_USER'),
            password=os.getenv('POSTGRES_PASSWORD')
        )

        with self.con.cursor() as cur:
            cur.execute('DROP TABLE IF EXISTS schema1.table1;')
            cur.execute('DROP TABLE IF EXISTS schema2.table2;')
            cur.execute('DROP TABLE IF EXISTS schema3.table3;')
            cur.execute('DROP SCHEMA IF EXISTS schema1;')
            cur.execute('DROP SCHEMA IF EXISTS schema2;')
            cur.execute('DROP SCHEMA IF EXISTS schema3;')

        self.con.commit()

        with self.con.cursor() as cur:
            cur.execute('CREATE SCHEMA IF NOT EXISTS schema1;')
            cur.execute('CREATE SCHEMA IF NOT EXISTS schema2;')
            cur.execute('CREATE SCHEMA IF NOT EXISTS schema3;')
            cur.execute('CREATE TABLE IF NOT EXISTS schema1.table1 (_id  serial primary key, custom_data  jsonb, field1  real, field2  text);')
            cur.execute('CREATE TABLE IF NOT EXISTS schema2.table2 (_id  serial primary key, custom_data  jsonb, field1  integer, field2  integer);')
            cur.execute('CREATE TABLE IF NOT EXISTS schema3.table3 (_id  serial primary key, custom_data  jsonb, field1  real, field2  text, field3  uuid, field4  timestamp, field5  date);')

        self.con.commit()

    def tearDown(self):

        with self.con.cursor() as cur:
            cur.execute('DROP TABLE IF EXISTS schema1.table1;')
            cur.execute('DROP TABLE IF EXISTS schema2.table2;')
            cur.execute('DROP TABLE IF EXISTS schema3.table3;')
            cur.execute('DROP SCHEMA IF EXISTS schema1;')
            cur.execute('DROP SCHEMA IF EXISTS schema2;')
            cur.execute('DROP SCHEMA IF EXISTS schema3;')

        self.con.commit()
        self.con.close()

    def test_to_postgres(self):

        d1 = Data1.create_empty()
        d1._id = 1
        d1.custom_data = {}
        d1.field1 = 5.0
        d1.field2 = 'hi'

        d2 = Data2.create_empty()
        d2._id = 2
        d2.custom_data = {'key1': 546, 'key2': uuid.uuid4()}
        d2.field1 = 4
        d2.field2 = d1

        d1.to_postgres(self.con, commitTransaction=True)
        d2.to_postgres(self.con, commitTransaction=True)

        with self.con.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.execute('SELECT * FROM schema1.table1 WHERE _id=1;')
            qryD1 = cur.fetchone()

            cur.execute('SELECT * FROM schema2.table2 WHERE _id=2;')
            qryD2 = cur.fetchone()

        self.assertEqual(d1.field1, qryD1['field1'])
        self.assertEqual(d2.field1, qryD2['field1'])
        self.assertEqual(d1._id, qryD2['field2'])

    def test_create_from_postgres(self):

        with self.con.cursor() as cur:
            cur.execute('CREATE SCHEMA IF NOT EXISTS schema1;')
            cur.execute('CREATE SCHEMA IF NOT EXISTS schema2;')
            cur.execute("INSERT INTO schema1.table1 (_id, custom_data, field1, field2) VALUES (5, '{}', 6.1, 'teststr');")
            cur.execute("INSERT INTO schema2.table2 (_id, custom_data, field1, field2) VALUES (3, '{}', 18, 5);")

        self.con.commit()

        d1 = Data1.create_empty()
        d1._id = 5
        d1.custom_data = {}
        d1.field1 = 6.1
        d1.field2 = 'teststr'

        d2 = Data2.create_empty()
        d2._id = 3
        d2.custom_data = {}
        d2.field1 = 18
        d2.field2 = d1

        self.assertEqual(d1, Data1.create_from_postgres(self.con, recordId=5))
        self.assertEqual(d2, Data2.create_from_postgres(self.con, recordId=3))

    def test_roundtrip_postgres(self):

        d1 = Data1.create_empty()
        d1.custom_data = {}
        d1.field1 = 6.1
        d1.field2 = 'teststr'

        d2 = Data2.create_empty()
        d2.custom_data = {}
        d2.field1 = 18
        d2.field2 = d1

        d1.to_postgres(self.con, commitTransaction=True)
        d2.to_postgres(self.con, commitTransaction=True)

        newD1 = Data1.create_from_postgres(self.con, recordId=d1._id)
        newD2 = Data2.create_from_postgres(self.con, recordId=d2._id)

        self.assertEqual(d1, newD1)
        self.assertEqual(d2, newD2)

class DataframeTest(unittest.TestCase):

    def test_create_dataframe(self):

        d11 = Data1.create_empty()
        d11._id = 11
        d11.custom_data = {}
        d11.field1 = 5.0
        d11.field2 = 'hi'

        d12 = Data1.create_empty()
        d12._id = 12
        d12.custom_data = {}
        d12.field1 = 75.0
        d12.field2 = 'hola'

        d21 = Data2.create_empty()
        d21._id = 21
        d21.custom_data = {}
        d21.field1 = 39
        d21.field2 = d11

        d22 = Data2.create_empty()
        d22._id = 22
        d22.custom_data = {}
        d22.field1 = 47
        d22.field2 = d12

        recs = [d21, d22]

        df = data_model.create_dataframe(recs)

        self.assertEqual(
            list(df.columns),
            [
                '_id',
                'custom_data',
                'field1',
                'field2._id',
                'field2.custom_data',
                'field2.field1',
                'field2.field2',
            ]
        )

        self.assertEqual(len(df), 2)

        df.set_index('_id', inplace=True)
        self.assertEqual(df.loc[22,'field1'], 47)
        self.assertEqual(df.loc[22,'field2.field2'], 'hola')

class CsvFileTest(unittest.TestCase):

    def test_write_csv_file(self):

        d11 = Data1.create_empty()
        d11._id = 11
        d11.custom_data = {'c1': 2}
        d11.field1 = 5.0
        d11.field2 = 'hi'

        d12 = Data1.create_empty()
        d12._id = 12
        d12.custom_data = {}
        d12.field1 = 75.0
        d12.field2 = 'hola'

        d21 = Data2.create_empty()
        d21._id = 21
        d21.custom_data = {}
        d21.field1 = 39
        d21.field2 = d11

        d22 = Data2.create_empty()
        d22._id = 22
        d22.custom_data = {}
        d22.field1 = 47
        d22.field2 = d12

        recs = [d21, d22]

        outStr = io.StringIO(newline='')
        data_model.write_csv_file(recs, outStr)

        self.assertEqual(
            outStr.getvalue().strip(),
            '_id,custom_data,field1,field2._id,field2.custom_data,field2.field1,field2.field2\r\n'
            '21,,39,11,"{""c1"": 2}",5.0,hi\r\n'
            '22,,47,12,,75.0,hola'
        )


if __name__ == '__main__':
    unittest.main()
