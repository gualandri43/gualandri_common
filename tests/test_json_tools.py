import os
import uuid
import datetime
import unittest

import gualandri_common.json_tools as json_tools


class JsonToolsTest(unittest.TestCase):

    def test_flatten_dict(self):

        d1 = {
            'k1': 134,
            'k2': 'jam',
            'k3': {
                'n1': 14,
                'n2': 'peanut butter'
            }
        }

        self.assertEqual(
            json_tools.flatten_dict(d1),
            {
                'k1': 134,
                'k2': 'jam',
                'k3.n1': 14,
                'k3.n2': 'peanut butter'
            }
        )


if __name__ == '__main__':
    unittest.main()
