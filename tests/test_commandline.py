import unittest
import argparse

import gualandri_common.commandline as commandline

TEST_PARSER = argparse.ArgumentParser(prog='testparser')

@commandline.create_subcommand(TEST_PARSER)
def cmd1(p1, p2):
    print(p1)
    print(p2)
    return None

class CommandlineTest(unittest.TestCase):

    def test_add_arguments_to_parser(self):

        args = {
            "--flag1": {
                "action": "store",
                "help": "a description of flag1"
            },
            "--flag2": {
                "action": "store",
                "help": "a description of flag1",
                "choices": ["valid1","valid2"]
            },
        }

        parser = argparse.ArgumentParser()
        commandline.add_arguments_to_parser(parser, args)

        parsed = parser.parse_args(["--flag1", "val1", "--flag2", "valid1"])

        self.assertEqual(parsed.flag1, "val1")
        self.assertEqual(parsed.flag2, "valid1")

    def test_add_subcommands_to_parser(self):

        commands = {
            "command1": {
                "description": "first command",
                "function": lambda x: print("test func"),
                "--flag1": {
                    "action": "store",
                    "help": "a description of flag1"
                },
                "--flag2": {
                    "action": "store",
                    "help": "a description of flag1",
                    "choices": ["valid1","valid2"]
                }
            },
            "command2": {
                "description": "second command",
                "--flag1": {
                    "action": "store",
                    "help": "a description of flag1"
                },
                "--flag2": {
                    "action": "store",
                    "help": "a description of flag1",
                    "choices": ["valid1","valid2"]
                }
            }
        }

        parser = argparse.ArgumentParser()
        commandline.add_subcommands_to_parser(parser, commands)

        parsed = parser.parse_args(["command1", "--flag1", "val1", "--flag2", "valid1"])

        self.assertEqual(parsed.flag1, "val1")
        self.assertEqual(parsed.flag2, "valid1")

        parsed = parser.parse_args(["command2", "--flag1", "val2", "--flag2", "valid2"])

        self.assertEqual(parsed.flag1, "val2")
        self.assertEqual(parsed.flag2, "valid2")

#    def test_create_subcommand(self):
#
#        print(TEST_PARSER.print_help())
#        parsed = TEST_PARSER.parse_args(["cmd1", "--p1", "val1", "--p2", "val2"])
#
#        self.assertEqual(parsed.p1, "val2")
#        self.assertEqual(parsed.p2, "val2")



if __name__ == '__main__':
    unittest.main()
