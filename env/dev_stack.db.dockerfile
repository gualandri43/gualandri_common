FROM postgres:12

ENV POSTGRES_USER devDbUser
ENV POSTGRES_PASSWORD devDbPassword
ENV POSTGRES_DB devDbUser

# sql and shell scripts in /docker-entrypoint-initdb.d/ will execute in alphabetical order using POSTGRES_USER
#   only runs if data directory is not available
#
# $POSTGRES_DB defines the database this will be created in
#

#COPY sportsModules/data_model/*.sql /docker-entrypoint-initdb.d/
SHELL ["/bin/bash", "-c"]
WORKDIR /
