# provide a docker image containing a python environment with all required packages.
#
#   defaults to opening an IPython shell inside the sports-analysis environment
#


FROM python:3.8-buster

#copy in the python environment definition and install
#
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt && rm requirements.txt

# default to a iPython terminal on start
CMD ["ipython"]
