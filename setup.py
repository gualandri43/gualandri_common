from setuptools import setup, find_packages


setup(
    name="gualandri_common",
    version="0.4.0",
    author="Daniel Gualandri",
    packages=find_packages(),
    package_data={
    },
    entry_points={
    }
)
