'''
support modules: common date and time functionality
'''

import datetime as dt


def record_time():
    '''
    provides a standardized recording of time.

    returns a dictionary filled with current time values. time recorded is the time
    of the call of this function.
        keys
        timeUtc
        timeLocal
        timeLocalString
    '''

    scrapeTimeUTC = dt.datetime.utcnow()
    scrapeTimeLocal = dt.datetime.now()

    timeD = {}
    timeD['timeUtc'] = scrapeTimeUTC
    timeD['timeLocal'] = scrapeTimeLocal
    timeD['timeLocalString'] = scrapeTimeLocal.strftime('%Y-%m-%d %H:%M:%S')

    return timeD

class TimeRecorder:
    '''
    provides a standardized recording of current time.
    '''

    def __init__(self):
        '''
        Records current time in the constructor.
        '''

        self.record()

    def record(self):
        '''
        re-record the current time
        '''

        self.timeUtc = dt.datetime.utcnow()
        self.timeLocal = dt.datetime.now()
        #self.timeLocalString = self.timeLocal.strftime('%Y-%m-%d %H:%M:%S')

    def seconds_since_last_record(self):
        '''
        Calculate the time elapsed (in seconds) since the last record().
        '''

        elapsed = self.time_since_last_record()
        return elapsed.total_seconds()

    def time_since_last_record(self):
        '''
        Calculate the time elapsed (as a timedelta object) since the last record().
        '''

        elapsed = dt.datetime.utcnow() - self.timeUtc
        return elapsed

    def to_dict(self):
        '''
        create a dict of the recorded attributes
        '''

        newDict = dict()

        newDict['timeUtc'] = self.timeUtc
        newDict['timeLocal'] = self.timeLocal
        #newDict['timeLocalString'] = self.timeLocalString

        return newDict

    def add_to_dict(self, target):
        '''
        add time entries to an existing dict

        target      An existing dictionary.
        '''

        if not isinstance(target, dict):
            raise ValueError("target is not a dictionary.")

        target.update(self.to_dict())

    def add_to_dataframe(self, dataFrame, prefix=None):
        '''
        add time info as columns to the given pandas dataframe.
        operates directly on given dataframe.
        '''

        if prefix is None:
            prefix = ''
        else:
            prefix += '.'

        dataFrame.loc[:, prefix + 'timeUtc'] = self.timeUtc
        dataFrame.loc[:, prefix + 'timeLocal'] = self.timeLocal
        #dataFrame.loc[:, prefix + 'timeLocalString'] = self.timeLocalString
