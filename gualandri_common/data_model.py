'''
Common data modeling functionality and base classes.
'''

import json
import csv
import dataclasses
import lxml
import psycopg2.extras
import pandas

import gualandri_common.json_tools


## register UUID type for usage in postgres
#
psycopg2.extras.register_uuid()
#
##

##
# custom json handling
#   (https://www.psycopg.org/docs/extras.html#json-adaptation)

class ExtendedJson(psycopg2.extras.Json):
    '''
        extend the JSON handler to handle additional
        datatypes dumped to json field.
    '''
    def dumps(self, obj):
        '''
            use the extended encoder class to help here.
        '''
        return json.dumps(obj, cls=gualandri_common.json_tools.ExtendedJsonEncoder)

#
#
##

@dataclasses.dataclass
class DataModelBase:
    '''
    Base class to use for making dataclasses. This base class provides functionality to
    save and load instances of the class from various data storage formats such as
    dict, json, postgresql, xml, csv.

    Subclass this and simply define the dataclass as normal.

    If database interop is desired, the subclass must define a _schema_name and _table_name class attributes.

    '''

    _schema_name = None
    _table_name = None

    _id: int
    custom_data: dict

    @classmethod
    def _get_data_types(cls):
        '''
        builds a dictionary with key: field name, value: data type
        '''
        return {x.name:x.type for x in dataclasses.fields(cls)}

    @staticmethod
    def _snake_to_camel_case(stringToConvert):
        '''
        convert snake_case to camelCase
        '''
        parts = stringToConvert.split('_')

        if len(parts) == 1:
            return stringToConvert[0].lower() + stringToConvert[1:]
        else:
            return parts[0].lower() + ''.join(x.title() for x in parts[1:])

    @staticmethod
    def _camel_to_snake_case(stringToConvert):
        '''
        convert camelCase to snake_case
        '''

        resultString = ''
        count = 0
        for ltr in stringToConvert:
            if ltr.isalpha() and ltr == ltr.upper() and count != 0:
                resultString += '_'

            resultString += ltr.lower()
            count += 1

        return resultString

    def is_default_value(self):
        '''
        returns True if all fields are at default value.
        '''

        for _field in dataclasses.fields(self):

            _subVal = getattr(self, _field.name)

            if dataclasses.is_dataclass(_field.type) and issubclass(_field.type, DataModelBase):
                # recurse into nested dataclasses

                _subValAtDefault = _subVal.is_default_value()

            else:

                _subValAtDefault = (_subVal == _field.default)

            if not _subValAtDefault:
                return False

        return True

    @classmethod
    def create_empty(cls):
        '''
        create an empty record.
        '''

        emptyParams = dict()
        for _field in dataclasses.fields(cls):

            if dataclasses.is_dataclass(_field.type):
                # recurse into nested dataclasses
                emptyParams[_field.name] = _field.type.create_empty()

            elif _field.type is dict:
                emptyParams[_field.name] = dict()

            else:
                emptyParams[_field.name] = None

        return cls(**emptyParams)

    @classmethod
    def create_from_existing(cls, other):
        '''
        create a new class instance, copying from existing class other.

        other       an instance of another DataModelBase object
        '''

        if not isinstance(other, DataModelBase):
            raise ValueError("other must be a subclass of DataModelBase.")

        return cls.create_from_dict(other.to_dict())

    def to_postgres(self, con, commitTransaction=False):
        '''
        save class to postgres database

        con         the psycopg2 connection object

        The _id field will be set to the row id when executing an insert.

        assumes the table id field is named '_id'.
        assumes attributes that are dataclasses are additional tables
            joined by foreign keys.
        assumes attributes match field names exactly in database
        '''


        #  make lists for the field names and value placeholders
        #
        #cur.execute("""
        #...     INSERT INTO some_table (an_int, a_date, another_date, a_string)
        #...     VALUES (%(int)s, %(date)s, %(date)s, %(str)s);
        #...     """,
        #...     {'int': 10, 'str': "O'Reilly", 'date': datetime.date(2005, 11, 18)})
        #
        #

        # gets a dictionary with all nested info
        #   will overwrite nested dataclass objects with ID reference
        #
        _values = self.to_dict()

        # iterate through fields and modify _values for values data types
        #   leave out any None fields (will be null in database)
        for _field in dataclasses.fields(self):

            _val = getattr(self, _field.name)

            if _val is None:
                # remove from _values dictionary
                _values.pop(_field.name)

            else:
                # field has a value

                if dataclasses.is_dataclass(_val):
                    # recurse into nested dataclasses
                    #   assumes each dataclass is a table
                    #   assign field to nested object _id
                    _val.to_postgres(con, commitTransaction=False)
                    _values[_field.name] = _val._id

                if isinstance(_val, dict):
                    # wrap a dictionary in the Json extension type for postgres storage
                    _values[_field.name] = ExtendedJson(_values[_field.name])


        with con.cursor() as curs:

            # make insert statment
            insertStatement = self.build_insert_sql_postgres(
                fieldsToInclude=list(_values.keys())
            )

            #send insert and retrieve newly inserted row id
            curs.execute(insertStatement, _values)
            self._id = curs.fetchone()[0]

        if commitTransaction:
            con.commit()

    @classmethod
    def build_insert_sql_postgres(cls, fieldsToInclude=None):
        '''
        build an INSERT (with ON CONFLICT UPDATE) SQL statement for
        postgresql.

        INSERT statment is complete with named placeholders to use with psycopg2.

        INSERT statment includes RETURNING clause to return the _id field.

        fieldsToInclude     A list of field names to include. If None, all are included.
        '''

        #if not defined, include all
        if fieldsToInclude is None:
            fieldsToInclude = [x.name for x in dataclasses.fields(cls)]

        if not isinstance(fieldsToInclude, list):
            raise ValueError('fieldsToInclude must be a list')

        # make insert statment
        insertStatement = 'INSERT INTO {0}.{1} ({2}) VALUES ({3}) ON CONFLICT (_id) DO UPDATE SET {4} RETURNING _id;'.format(
            cls._schema_name,
            cls._table_name,
            ','.join(fieldsToInclude),
            ','.join(['%({0})s'.format(x) for x in fieldsToInclude]),
            ','.join(['{0}=EXCLUDED.{0}'.format(x) for x in fieldsToInclude])
        )

        return insertStatement.strip()

    @classmethod
    def build_select_sql_postgres(cls, recordId=None):
        '''
        create a SELECT SQL statment

        recordId        Either a single value or list of IDs to query.

        '''

        ## build joins for nested dataclasses
        ##SELECT * FROM tablename JOIN other ON (tablename.field=other._id) WHERE tablename._id IN ();
        # joinList = list()
        # for _field in dataclasses.fields(cls):

        #     if dataclasses.is_dataclass(_field.type):
        #         joinList.append(
        #             'JOIN {0}.{1} AS {1} ON ({1}._id={2}.{3})'.format(
        #                 _field.type._schema_name,
        #                 _field.type._table_name,
        #                 cls._table_name,
        #                 _field.name
        #                 )
        #             )

        whereClause = None
        if recordId:

            if isinstance(recordId, list):
                whereClause = 'WHERE _id IN ({0})'.format(','.join(recordId))
            else:
                whereClause = 'WHERE _id={0}'.format(recordId)


        insertSql = 'SELECT * FROM {0}.{1} AS {1}'.format(
            cls._schema_name,
            cls._table_name
        )

        if whereClause:
            insertSql += ' ' + whereClause

        insertSql += ';'

        return insertSql.strip()

    @classmethod
    def create_from_postgres(cls, con, recordId=None):
        '''
        build class or list of classes from database.

        recordId        Either a single value or list of IDs to query.
        '''

        with con.cursor() as curs:

            # build and execute SELECT statement
            curs.execute(cls.build_select_sql_postgres(recordId=recordId))

            results = list()

            for fetchRecord in curs.fetchall():

                record = dict()
                colIdx = 0
                for col in curs.description:
                    # iterate through column descriptions
                    # name is 0th item of column description
                    record[str(col[0])] = fetchRecord[colIdx]

                    colIdx += 1

                # update record dict with special field handling
                for _field in dataclasses.fields(cls):

                    if dataclasses.is_dataclass(_field.type):
                        # recurse into nested dataclasses
                        #   assumes each dataclass is a table
                        # build nested object, get a dictionary, insert into record dict
                        #
                        # this should handle multiple levels of nested objects
                        #
                        subObj = _field.type.create_from_postgres(con, recordId=record[_field.name])
                        record[_field.name] = subObj.to_dict()

                #create new class
                results.append(cls.create_from_dict(record))

        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0]
        else:
            return results

    def to_dict(self):
        '''
        make data into a dictionary.
        '''
        return dataclasses.asdict(self)

    @classmethod
    def create_from_dict(cls, record, keyMapping=None):
        '''
        Build the class from a dictionary.

        Adds any key in the input record that doesn't correspond
        to a dataclass field to the custom_data dictionary.

        keyMapping      An optional dictionary that defines a mapping
                        between the key in the given input record and the
                        corresponding field name in the dataclass, if they are
                        different. Useful when keys in record are different from
                        expected. Key: key name in record, Value: dataclass field name.
        '''

        if not isinstance(record, dict):
            raise ValueError("input record must be a dictionary.")

        params = dict()
        params['custom_data'] = dict()

        # use for getting field info by field name
        definedFields = {x.name:x for x in dataclasses.fields(cls)}

        # iterate through each field in input record
        for _key in record:

            # look for a defined key mapping and update if needed
            recordKey = _key
            if keyMapping:
                if recordKey in keyMapping:
                    recordKey = keyMapping[recordKey]

            # get the value or object of the field
            #
            fieldValue = None
            fieldValue = record[_key]

            if recordKey in definedFields:

                fieldInfo = definedFields[recordKey]

                if dataclasses.is_dataclass(fieldInfo.type):
                    # recurse into nested dataclasses

                    if issubclass(fieldInfo.type, DataModelBase):
                        fieldValue = fieldInfo.type.create_from_dict(record[_key])


            # assign to parameters if the field exists in the dataclass,
            #   or add to custom_data if it doesn't
            #
            if recordKey in definedFields:
                params[recordKey] = fieldValue
            else:
                params['custom_data'][recordKey] = fieldValue

        # fill not included fields with None
        for _fieldName in definedFields:
            if _fieldName not in params:
                params[_fieldName] = None

        # construct the class
        return cls(**params)

    def to_json(self):
        '''make json string with data.'''
        return json.dumps(self.to_dict(), cls=gualandri_common.json_tools.ExtendedJsonEncoder)

    @classmethod
    def create_from_json(cls, jsonString):
        '''
        Build the class from a json string
        '''
        # use for getting field info by field name
        #definedFields = {x.name:x for x in dataclasses.fields(cls)}

        # load json string into dictionary
        #   at first, more complicated entries are strings (UUIDs, datetimes, etc.)
        #   then create a new dictionary that creates objects from string reps
        #   use create_from_dict() to handle the rest
        #
        data = json.loads(jsonString)
        typeInfo = cls._get_data_types()

        deserializedData = {
            _field:gualandri_common.json_tools.extended_json_decoding(_data, typeInfo.get(_field))
            for (_field,_data) in data.items()
        }

        return cls.create_from_dict(deserializedData)

    def to_xml(self):
        '''
        create an lxml xml element. the tag name is the name of the class.
        '''

        elem = lxml.etree.Element(type(self).__name__)
        elem.set('_id', str(self._id))

        for _field in dataclasses.fields(self):
            if _field.name != '_id':
                subElem = lxml.etree.SubElement(elem, _field.name)

                if dataclasses.is_dataclass(_field.type):
                    _val = getattr(self, _field.name)
                    if isinstance(_val, DataModelBase):
                        subElem.append(_val.to_xml())
                else:
                    subElem.text = str(getattr(self, _field.name))

        return elem

    @classmethod
    def build_csv_writer(cls, outFile, dialect='excel'):
        '''
        Create a csv.DictWriter object that will output class data as a csv file.

        outFile     Any file-like object to write the csv data to.
        dialect     the csv.Dialect class format to use. another option is 'unix_dialect'.

        returns an initialized csv.DictWriter object and writes the header row.
        '''

        empty = cls.create_empty()
        flatEmpty = gualandri_common.json_tools.flatten_dict(empty.to_dict(), skipKeys=['custom_data'])

        writer = csv.DictWriter(outFile, [str(x) for x in flatEmpty.keys()], dialect=dialect)
        writer.writeheader()
        return writer

    def to_csv(self, outWriter):
        '''
        output to csv format. Adds a single row to an existing csv writer

        outWriter     A csv.DictWriter class to add the object data to. May
                      be created using the build_csv_writer() method.
        '''

        if not isinstance(outWriter, csv.DictWriter):
            raise ValueError('outWriter must be a csv.DictWriter object.')

        # serialize to JSON to format nonstandard datatypes to string properly
        #   load back into dictionary
        jsonOutput = self.to_json()
        data = json.loads(jsonOutput)
        flattened = gualandri_common.json_tools.flatten_dict(data, skipKeys=['custom_data'])

        # dictionary now in format to serialize.
        outWriter.writerow(flattened)


def create_dataframe(dataRecords):
    '''
    create a dataframe from a list of DataModelBase records.
    '''

    if not isinstance(dataRecords, list):
        return ValueError('dataRecords must be a list.')

    if not all([isinstance(x, DataModelBase) for x in dataRecords]):
        return ValueError('dataRecords components must all be subclasses of DataModelBase.')

    classOfRecord = type(dataRecords[0])

    if not all([isinstance(x, classOfRecord) for x in dataRecords]):
        return ValueError('dataRecords components must all be the same type.')

    recs = [gualandri_common.json_tools.flatten_dict(x.to_dict()) for x in dataRecords]

    return pandas.DataFrame.from_dict(recs, orient='columns')

def write_csv_file(dataRecords, outFile, dialect='excel'):
    '''
        Right a set of records to a csv file.

        outFile     file-like object
        dialect     the csv.Dialect class format to use. another option is 'unix_dialect'.
    '''

    if not isinstance(dataRecords, list):
        return ValueError('dataRecords must be a list.')

    if not all([isinstance(x, DataModelBase) for x in dataRecords]):
        return ValueError('dataRecords components must all be subclasses of DataModelBase.')

    classOfRecord = type(dataRecords[0])

    if not all([isinstance(x, classOfRecord) for x in dataRecords]):
        return ValueError('dataRecords components must all be the same type.')


    writer = classOfRecord.build_csv_writer(outFile, dialect=dialect)
    for _record in dataRecords:
        _record.to_csv(writer)
