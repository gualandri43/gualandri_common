import json
import uuid
import datetime


class ExtendedJsonEncoder(json.JSONEncoder):
    '''
    Add serialization formatting for nonstandard classes and types,
    such as UUIDs, datetimes, etc.
    '''

    def default(self, o):

        if isinstance(o, uuid.UUID):
            # UUID serialization as hex string
            return str(o)

        elif isinstance(o, datetime.date) or isinstance(o, datetime.datetime) or isinstance(o, datetime.time):
            # date, datetime, time serialization in ISO 8601 format
            return o.isoformat()

        elif isinstance(o, datetime.timedelta):
            # timedelta serialization to seconds
            return o.total_seconds()

        else:
            # use default encoder for all others
            return super().default(o)

def extended_json_decoding(obj, targetType):
    '''
    decode a string representation into an object. use for nonstandard classes/types

    targetType      the type of the object to convert string to (use type() or similar)
    obj             obj is assumed to be a serialized string if it is one of the handled types
    '''

    if targetType is None:
        return obj

    if targetType is uuid.UUID:
        return uuid.UUID(obj)

    elif targetType is datetime.date:
        return datetime.date.fromisoformat(obj)

    elif targetType is datetime.datetime:
        return datetime.datetime.fromisoformat(obj)

    elif targetType is datetime.time:
        return datetime.time.fromisoformat(obj)

    elif targetType is datetime.timedelta:
        return datetime.timedelta(seconds=int(obj))

    else:
        return obj

def flatten_dict(targetDict, skipKeys=None):
    '''
    flatten a dictionary with nested object. nested keys become <parent.child> format.

    targetDict      dictionary to flatten
    skipKeys        an optional list of keys that will not be recursed into.
                    if the value is a dictionary, it will be dumped to a json string.
    '''

    if skipKeys is None:
        skipKeys = list()

    newDict = dict()
    for k,v in targetDict.items():

        if isinstance(v, dict):
            if len(v) > 0:
                if k in skipKeys:
                    newDict[k] = json.dumps(v, cls=ExtendedJsonEncoder)
                else:
                    nested = flatten_dict(v, skipKeys=skipKeys)
                    newDict.update({'{0}.{1}'.format(k,_nestK):_nestVal for (_nestK,_nestVal) in nested.items()})
            else:
                newDict[k] = None

        else:
            newDict[k] = v

    return newDict
