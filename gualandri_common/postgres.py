'''
helper functionality for PostgreSQL databases.
'''

import psycopg2


class DatabaseInfo:
    '''
    Provides an interface for checking Postgres Database metadata.
    '''

    def __init__(self, username, password, host, port=5432):

        self.tables = set()
        self.schemas = set()

        conn = psycopg2.connect(username=username, password=password, host=host, port=port)

        with conn.cursor() as curs:
            curs.execute("SELECT schema_name FROM information_schema.schemata;")

            for rec in curs.fetchall():
                self.schemas.add(rec[0])

            curs.execute("SELECT table_schema,table_name FROM information_schema.tables;")

            for rec in curs.fetchall():
                self.tables.add('{0}.{1}'.format(rec[0], rec[1]))

        conn.close()

    def check_if_schema_exists(self, schema):
        '''
        schema      name of schema
        '''

        if schema in self.schemas:
            return True
        else:
            return False

    def check_if_table_exists(self, schema, table):
        '''
        schema      name of schema
        table       name of table
        '''

        chkName = '{0}.{1}'.format(schema, table)

        if chkName in self.tables:
            return True
        else:
            return False

