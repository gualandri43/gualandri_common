import logging


def get_python_log_level(stringLogLevel):
    '''
    convert from string to python logging library level

    stringLogLevel:     A string representation of the logging library levels
                        'CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'
    '''

    if stringLogLevel == 'CRITICAL':
        return logging.CRITICAL

    if stringLogLevel == 'ERROR':
        return logging.ERROR

    if stringLogLevel == 'WARNING':
        return logging.WARNING

    if stringLogLevel == 'INFO':
        return logging.INFO

    if stringLogLevel == 'DEBUG':
        return logging.DEBUG

    #default
    return logging.DEBUG

def set_up_console_logger(loggerName, logLevel=logging.INFO):
    '''
    Convenience function to add a standard formatted console log handler to
    a given loggerName. Run once, and the loggerName will remain set up correctly.

    retrieve the logger from a script or module with logging.getLogger(loggerName)

    Only run once per loggerName
    '''

    logger = logging.getLogger(loggerName)

    formatter = logging.Formatter('[%(asctime)s | %(name)s] [%(levelname)s]  %(message)s')
    console = logging.StreamHandler()
    console.setLevel(get_python_log_level(logLevel))
    console.setFormatter(formatter)

    logger.addHandler(console)
    logger.setLevel(get_python_log_level(logLevel))   #might have to adjust this part
                                                                # to allow for adding multiple handlers to log
