'''
Module for helper functions related to generating and parsing command line.
'''

import argparse
import functools
import inspect

def add_arguments_to_parser(parser, argumentDefinition):
    '''
    Add a set of arguments to an ArgumentParser based on a dictionary definition
    of the arguments and options.

    parser                  an instance of an argparge.ArgumentParser object
    argumentDefinition      dictionary defining commands to add and the associated options.

    The format for argumentDefinition is as follows. The keys are the argument names and the value
    is a dictionary with keys that match the arguments of argparse.ArgumentParser.add_argument().
    See the argparse documentation for info about the keywords.

    {
        "--optionFlag1": {
            action: "store",
            nargs,
            const,
            default,
            type,
            choices,
            required,
            help,
            metavar,
            dest
        },
        "--optionFlag2": {
            action: "store",
            nargs,
            const,
            default,
            type,
            choices,
            required,
            help,
            metavar,
            dest
        },
        ...
    }
    '''

    if not isinstance(parser, argparse.ArgumentParser):
        raise ValueError("parser must be an argparse.ArgumentParser object.")

    for argName,opts in argumentDefinition.items():
        parser.add_argument(argName, **opts)

def add_subcommands_to_parser(parser, subcommandDefinitions):
    '''
    Add a set of subcommands to an ArgumentParser based on a dictionary definition
    of the subcommands, and their arguments and options.

    parser                      an instance of an argparge.ArgumentParser object
    subcommandDefinitions       dictionary defining commands to add and the associated options.

    The format for subcommandDefinitions is as follows. The first keys are the subcommand names.
    Each of those has a sub dictionary with keys of the argument names and the value
    is a dictionary with keys that match the arguments of argparse.ArgumentParser.add_argument().
    Additionally, a key/value pair for each command may be used to define the help
    description and the function to associate with each subcommand.
    See the argparse documentation for info about the keywords.

    {
        "command1":{
            "description": "A description of the command.",
            "function": do_something,
            "--optionFlag1": {
                action: "store",
                nargs,
                const,
                default,
                type,
                choices,
                required,
                help,
                metavar,
                dest
            },
            "--optionFlag2": {
                action: "store",
                nargs,
                const,
                default,
                type,
                choices,
                required,
                help,
                metavar,
                dest
            },
            ...
        },
        ...
    }
    '''

    if not isinstance(parser, argparse.ArgumentParser):
        raise ValueError("parser must be an argparse.ArgumentParser object.")

    subparsers = parser.add_subparsers()

    for commandName,arguments in subcommandDefinitions.items():

        commandDescription = arguments.pop("description", None)
        commandFunction = arguments.pop("function", None)

        subparser = subparsers.add_parser(commandName, help=commandDescription)
        add_arguments_to_parser(subparser, arguments)

        if commandFunction:
            subparser.set_defaults(func=commandFunction)

def create_subcommand(parser):
    '''
    This is a decorator to generate a subcommand from a function and
    add it to an existing argparse.ArgumentParser class.
    '''

    def decorator_create_subcommand(f):
        '''
        decorator to turn a function into a argparse subcommand.
        '''

        @functools.wraps(f)
        def wrapper_create_subcommand():
            print(f.__name__)
            subparsers = parser.add_subparsers()
            subparser = subparsers.add_parser(f.__name__)

            for argName in inspect.getargspec(f).args:
                subparser.add_argument('--' + argName)

            subparser.set_defaults(func=f)

        return wrapper_create_subcommand
    return decorator_create_subcommand
